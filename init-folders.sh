#!/usr/bin/env bash

mkdir -p /elister/angular
mkdir -p /elister/api
mkdir -p /elister/mongo
mkdir -p /elister/proxy
mkdir -p /elister/certbot/conf
mkdir -p /elister/certbot/www
