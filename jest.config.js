module.exports = {
  projects: [
    '<rootDir>/apps/simple-angular-nextjs',
    '<rootDir>/apps/api',
    '<rootDir>/apps/admin',
  ],
};
