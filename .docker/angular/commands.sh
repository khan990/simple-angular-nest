#!/bin/sh

pm2-runtime start /elister/dist/simple-angular-nextjs/server/main.js --output /elister/logs/client.log &

nginx -g 'daemon off;'

