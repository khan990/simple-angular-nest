#!/usr/bin/env bash

mkdir -p /elister/angular
mkdir -p /elister/api
mkdir -p /elister/proxy
mkdir -p /elister/mongo
mkdir -p /elister/certbot-var
mkdir -p /elister/certbot-etc
