#!/usr/bin/env bash

sudo npm run build:ssr --prod
sudo ng build admin --prod


docker build . -f ./.docker/angular/Dockerfile -t registry.gitlab.com/khan990/simple-angular-nest/angular:0.0.1-alpha.1
# docker push registry.gitlab.com/khan990/simple-angular-nest/angular:0.0.1-alpha.1

sudo ng build api --prod

docker build . -f ./.docker/api/Dockerfile -t registry.gitlab.com/khan990/simple-angular-nest/api:0.0.1-alpha.1
# docker push registry.gitlab.com/khan990/simple-angular-nest/api:0.0.1-alpha.1

docker build . -f ./.docker/nginx/Dockerfile -t registry.gitlab.com/khan990/simple-angular-nest/nginx:0.0.1-alpha.1
# docker push registry.gitlab.com/khan990/simple-angular-nest/nginx:0.0.1-alpha.1

