import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from '@simple-angular-nest/api-interfaces';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';

@Component({
  selector: 'simple-angular-nest-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  helloUrl: string;
  hello$: Observable<any>;

  constructor(private http: HttpClient) {
    if (environment.production) {
      this.helloUrl = `http://server.calculate-all.com/api/hello`;
    } else {
      this.helloUrl = `http://localhost:3333/api/hello`;
    }

    this.hello$ = this.getHello();
  }

  private getHello(): Observable<any> {
    return this.http.get<Message>(this.helloUrl);
  }
}
