import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { Message } from '@simple-angular-nest/api-interfaces';

import { AppService } from './app.service';
import { StudentDto } from './db/student.models';
import { StudentService } from './db/student.service';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService, private studentDbService: StudentService, private eventEmitter: EventEmitter2) {}

  @Get('hello')
  getData(): Message {
    return this.appService.getData();
  }

  @Post('save-student')
  saveStudent(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body
  ) {
    console.log(body);
    const student: StudentDto = body;
    this.studentDbService.create(student);
    res.status(200).send();
  }

  @Get('get-students')
  async getStudents(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body
  ) {
    const students = await this.studentDbService.findAll();
    console.log(students)
    res.status(200).send(students);
  }

  @Get('trigger-event')
  async triggerEvent(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body
  ) {
    this.eventEmitter.emit('order.created', {name: 'some name', age: 50});
    res.status(200).send();
  }
}
