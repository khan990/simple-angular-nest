import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Student, StudentDocument } from './student.schema';
import { StudentDto } from './student.models';

@Injectable()
export class StudentService {
  constructor(@InjectModel(Student.name) private catModel: Model<StudentDocument>) {}

  async create(createCatDto: StudentDto): Promise<Student> {
    const createdCat = new this.catModel(createCatDto);
    return createdCat.save();
  }

  async findAll(): Promise<Student[]> {
    return this.catModel.find().exec();
  }
}
