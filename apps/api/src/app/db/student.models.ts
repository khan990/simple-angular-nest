export class StudentDto {
  name: string;
  age: number;
  score: number;
}
