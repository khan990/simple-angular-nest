import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventEmitterModule } from '@nestjs/event-emitter';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StudentsModule } from './db/student.module';

import { environment } from '../environments/environment';

let mongoServerName;

if (environment.production) {
  mongoServerName = 'elister-mongo';
} else {
  mongoServerName = 'localhost';
}
@Module({
  imports: [MongooseModule.forRoot(`mongodb://${mongoServerName}/nest`), StudentsModule, EventEmitterModule.forRoot()],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
