import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { Message } from '@simple-angular-nest/api-interfaces';

@Injectable()
export class AppService {
  getData(): Message {
    return { message: 'Welcome to api!' };
  }

  @OnEvent('order.created')
  handleOrderCreatedEvent(payload: any) {
    console.log('Event is triggered...');
    console.log(payload);
  }
}
